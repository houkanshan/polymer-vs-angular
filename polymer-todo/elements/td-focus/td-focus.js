(function() {
  Polymer('td-focus', {
    model: null
  , modelIdChanged: function(a, b) {
      this.model = document.querySelector('#' + this.modelId)
      this.model.focusIndex = 0
    }
  , getFocusItem: function() {
      return this.model.filtered[this.model.focusIndex]
    }
  , focusDown: function() {
      this.focusTo(this.model.focusIndex + 1)
    }
  , focusUp: function() {
      this.focusTo(this.model.focusIndex - 1)
    }
  , focusTo: function(index) {
      var listLength = this.model.filtered.length
      if (index >= 0 && index < listLength) {
        this.model.focusIndex = index
      }
    }
  , toggleComplete: function() {
      var focusItem = this.getFocusItem()
      focusItem.completed = !focusItem.completed
    }
  , insertMode: function() {
      var focusItem = this.getFocusItem()
      focusItem.editing = true
      console.log('insertMode')
    }
  })
}());
