(function() {
  var inputNodeName = {
    'INPUT': 'input'
  , 'TEXTAREA': 'textarea'
  }

  Polymer('key-observer', {
    targetSel: ''
  , targetEl: null
  , ignoreInput: true
  , created: function() {
    }
  , ready: function() {
    }
  , targetSelChanged: function() {
      this.targetElem = document.querySelector(this.targetSel)
      this.stopListening()
      this.startListening()
    }
  , targetElChanged: function() {
      this.targetElem = this.targetEl
      this.stopListening()
      this.startListening()
    }
  , stopListening: function() {
    }
  , startListening: function() {
      this.targetElem.addEventListener('keydown', this.listenKeyDown.bind(this))
      this.targetElem.addEventListener('keyup', this.listenKeyUp.bind(this))
    }
  , listenKeyDown: function(e) {
      if (this.ignoreInput && e.srcElement.nodeName in inputNodeName) {
        return
      }
      switch(e.keyCode) {
        case 74: // j
        case 40:
          this.fire('key-down', e)
          break;
        case 75: // k
        case 38:
          this.fire('key-up', e)
          break;
        case 68:
          this.fire('key-d', e)
          break;
        case 27:
          this.fire('key-esc', e)
          break;
        case 73:
          this.fire('key-i', e)
          break;
      }
    }
  , listenKeyUp: function(e) {
    }
  })
}());
